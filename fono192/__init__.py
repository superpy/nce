# nce.fono192.__init__.py
"""
Fonoaudiologia Computacional 2019/2.

Protótipo inicial com interface de linha
Implementa o jogo do Tesouro Inca, explorando as ruínas de um templo perdido
"""
__author__ = "Carlo E T Oliveira <carlo at nce ufrj br>"
__version__ = "19.10.27"